# MySQL

To run a local MySQL for development purposes

Create a `.env` file with the relevant credentials

```
MYSQL_ROOT_PASSWORD=***
MYSQL_USER=siracusa
MYSQL_PASSWORD=***
```

Launch the container: `docker-compose -f mysql.yml up -d`

Test connection: `mysql -u root -p -h 127.0.0.1`

Create the database: `create database ariasiracusa;`

Assign privileges: `grant all privileges on ariasiracusa.* to siracusa@'%';`

Create schema: `mysql -u siracusa -p -h 127.0.0.1 ariasiracusa < schema.sql`

from google.oauth2 import service_account
from google.cloud import storage
from tabula import read_pdf
import pandas as pd
import numpy as np
import re 
import os
import glob
from datetime import date, timedelta, datetime
import locale
import ftputil
import mysql.connector
from dotenv import load_dotenv

root_dir = "aria-siracusa"
temp_bucket_name = root_dir
public_bucket_name = 'dati-aria-siracusa'
columns = ["date","type","location","so2_max","so2_max_time","so2_mean","co_max_8h","co_max_time","no2_max","no2_max_time","no2_mean","o3_max","o3_max_time","o3_mean","o3_max_8h","pm10","c6h6"]
ftp_server = "213.82.231.194"
status_file = "status.txt"
load_dotenv()


def mysqlConnect():
    return mysql.connector.connect(user=os.getenv('MYSQL_USER'), password=os.getenv('MYSQL_PASSWORD'), database='ariasiracusa')


def mysqlImport():
    global root_dir
    if os.path.isfile('%s/stations.csv' % root_dir):
        conn = mysqlConnect()
        stations = pd.read_csv('%s/stations.csv' % root_dir)
        records = []
        for index, row in stations.iterrows():
            records.append((row.network,row.location,row.address,row.lat,row.long,row.height))
        insert_query = "INSERT INTO stations (`network`, `location`, `address`, `lat`, `long`, `height`) VALUES (%s, %s, %s, %s, %s, %s) "
        cursor = conn.cursor()
        cursor.executemany(insert_query, records)
        conn.commit()
        print(cursor.rowcount, "records inserted successfully")
        years = sorted(glob.glob("%s/20*" % root_dir))
        for year_file in years:
            reports = pd.read_csv(year_file)
            mysqlUpdate(reports,conn)
        conn.close()
    else:
        print('No files to import in aria-siracusa')


def mysqlUpdate(reports):
    conn = mysqlConnect()
    reports = reports.where(pd.notnull(reports), None)
    records = []
    for index, row in reports.iterrows():
        records.append((row.date,row.type,row.location,row.so2_max,row.so2_max_time,row.so2_mean,row.co_max_8h,row.co_max_time,row.no2_max,row.no2_max_time,row.no2_mean,row.o3_max,row.o3_max_time,row.o3_mean,row.o3_max_8h,row.pm10,row.c6h6))
    insert_query = "INSERT INTO reports (`date`, `type`, `location`, `so2_max`, `so2_max_time`, `so2_mean`, `co_max_8h`, `co_max_time`, `no2_max`, `no2_max_time`, `no2_mean`, `o3_max`, `o3_max_time`, `o3_mean`, `o3_max_8h`, `pm10`, `c6h6`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) "
    cursor = conn.cursor()
    cursor.executemany(insert_query, records)
    conn.commit()
    conn.close()
    print("%s records inserted successfully" % (cursor.rowcount))


def processDirectory():
    global root_dir
    years = glob.glob("%s/*" % root_dir)
    for year in years:
        y = re.match(r"(.*)ANNO_(.*)$", year)
        current_year = y[2]
        data = []
        months = glob.glob("%s/*" % year)
        for month in months:
            days = glob.glob("%s/*qualita*" % month)
            for day in days:
                day_data = processPdf(day)
                data.extend(day_data)
        df = pd.DataFrame(data=data, columns=columns)
        df = df.replace('NA',np.NaN)
        df.to_csv("temp/%s.csv" % current_year, index=False)


def processPdfSave(filename, current_year):
    data = processPdf(filename)
    v = re.match(r".*aria_(.*)\.pdf", filename)
    df = pd.DataFrame(data=data, columns=columns)
    df = df.replace('NA',np.NaN)
    local_filename = "%s.csv" % v[1]
    df.to_csv("temp/"+local_filename, index=False)
    # insert into local database
    mysqlUpdate(df)
    filename_year = "temp/%s.pkl" % current_year
    # init new year pickle if not present
    if not os.path.isfile(filename_year):
        df_init = pd.DataFrame(columns=columns)
        df_init.to_pickle(filename_year)
    # load year pickle
    df_year = pd.read_pickle(filename_year)
    # append data
    df_year = df_year.append(df)
    # save year csv
    df_year.to_csv("temp/%s.csv" % current_year, index=False)
    # save yearly data in pickle
    df_year.to_pickle(filename_year)
    return local_filename


def processPdf(filename):
    print("Processing %s ..." % filename)
    data = []
    pdf_table = read_pdf(filename, stream=True, pages=1)
    df = pdf_table[0]
    for index, row in df.iterrows():
        if pd.notna(row[0]) and row[0][0] != '' and row[0][0] == '(':
            rowlist = row.to_numpy()
            v = re.match(r".*aria_(.*)\.pdf", filename)
            date = v[1].replace('_','-')
            rowlist = np.insert(rowlist, 0, date)
            if ' ' in rowlist[1] == True:
              explode = rowlist[1].split()
            else:
              explode = rowlist[1].split(')')
            rowlist = np.insert(rowlist, 1, explode[0].strip('()'))
            rowlist = np.insert(rowlist, 2, ' '.join(explode[1:]).strip())
            rowlist = np.delete(rowlist, 3)

            rowlist = np.insert(rowlist, 3, 'NA')
            rowlist = np.insert(rowlist, 4, 'NA')
            if rowlist[5].__contains__(' '):
                explode = rowlist[5].split()
                rowlist[3] = explode[0].replace(',','.')
                rowlist[4] = explode[1].strip('()')
            rowlist = np.delete(rowlist, 5)

            rowlist[5] = rowlist[5].replace(',','.').replace('N.D.','NA').replace('N.P.','NA')

            rowlist = np.insert(rowlist, 6, 'NA')
            rowlist = np.insert(rowlist, 6, 'NA')
            if rowlist[8].__contains__(' '):
                explode = rowlist[8].split()
                rowlist[6] = explode[0].replace(',','.')
                rowlist[7] = explode[1].strip('()')
            rowlist = np.delete(rowlist, 8)

            rowlist = np.insert(rowlist, 8, 'NA')
            rowlist = np.insert(rowlist, 8, 'NA')
            if rowlist[10].__contains__(' '):
                explode = rowlist[10].split()
                rowlist[8] = explode[0].replace(',','.')
                rowlist[9] = explode[1].strip('()')
            rowlist = np.delete(rowlist, 10)

            rowlist[10] = rowlist[10].replace(',','.').replace('N.D.','NA').replace('N.P.','NA')

            rowlist = np.insert(rowlist, 11, 'NA')
            rowlist = np.insert(rowlist, 11, 'NA')
            if rowlist[13].__contains__(' '):
                explode = rowlist[13].split()
                rowlist[11] = explode[0].replace(',','.')
                rowlist[12] = explode[1].strip('()')
            rowlist = np.delete(rowlist, 13)

            rowlist = np.insert(rowlist, 13, 'NA')
            rowlist = np.insert(rowlist, 13, 'NA')
            if rowlist[15].__contains__(' '):
                explode = rowlist[15].split()
                rowlist[13] = explode[0].replace(',','.')
                rowlist[14] = explode[1].strip('()')
            rowlist = np.delete(rowlist, 15)

            if len(rowlist) == 18:
                # occasional pm 2.5. to remove
                rowlist = np.delete(rowlist, 15)

            rowlist[15] = rowlist[15].replace(',','.').replace('N.D.','NA').replace('N.P.','NA')
            
            rowlist[16] = rowlist[16].replace(',','.').replace('N.D.','NA').replace('N.P.','NA')
            data.append(rowlist)
    return data

def processToday():
    data = ''
    with open(status_file, 'r') as file:
        data = file.read().replace('\n', '')

    if data != '':
        locale.setlocale(locale.LC_ALL, 'it_IT.utf8')
        last_day = datetime.strptime(data,"%Y_%m_%d")
        today = datetime.today()
        diff = (today - last_day).days
        for i in range(1,diff):
            current_day = last_day + timedelta(i)
            current_month = current_day.strftime("%B").capitalize()
            current_year = current_day.strftime("%Y")
            current_date = current_day.strftime("%Y_%m_%d")
            # check archive
            directory = "ARCHIVIO/ANNO_%s/%s" % (current_year, current_month)
            filename = "Reportgiornalieroqualitadellaria_%s.pdf" % current_date
            processFtpFile(directory, filename, current_date, current_year)
        # check today
        if diff > 1:
            processFtpFile("Ultimo Bollettino Giornaliero", filename, current_date, current_year)

def processFtpFile(directory, filename, current_date, current_year):
    # check file exists
    host = ftputil.FTPHost(ftp_server, 'anonymous', 'password')
    host.chdir(directory)
    files = host.listdir(host.curdir)
    if filename in files:
        print("Downloading %s ..." % filename)
        filename_temp = "temp/" + filename
        host.download(filename, filename_temp)
        # process
        local_filename = processPdfSave(filename_temp, current_year)
        # upload to gs bucket
        uploadToBucket(temp_bucket_name, "temp/"+local_filename, "csv/"+local_filename)
        uploadToBucket(public_bucket_name, "temp/%s.csv"  % current_year, "%s.csv" % current_year)
        # clean up
        os.remove("temp/"+local_filename)
        os.remove(filename_temp)
        # update status
        with open(status_file, "w") as text_file:
            text_file.write(current_date)
    host.close()

def uploadToBucket(bucket_name, local_filename, remote_filename):
    credentials = service_account.Credentials.from_service_account_file(
        'key.json', scopes=["https://www.googleapis.com/auth/cloud-platform"],
    )
    gs_client = storage.Client(credentials=credentials)
    bucket = gs_client.bucket(bucket_name)
    blob = bucket.blob(remote_filename)
    blob.upload_from_filename(local_filename)
    print("File {} uploaded to gs://{}/{}".format(local_filename, bucket_name, remote_filename))

# Process archive
# processDirectory()

# Import data in MySQL
# mysqlImport()

# Test with single file
# processPdfSave("aria-siracusa/ANNO_2021/Gennaio/Reportgiornalieroqualitadellaria_2021_01_06.pdf")

# Daily process
processToday()

DROP TABLE IF EXISTS `stations`;

CREATE TABLE `stations` (
    `network` varchar(64) NOT NULL DEFAULT '',
    `location` varchar(64) NOT NULL DEFAULT '',
    `address` varchar(255) NOT NULL DEFAULT '',
    `lat` decimal(10,8) NOT NULL,
    `long` decimal(11,8) NOT NULL,
    `height` decimal(4,2) NOT NULL,
    PRIMARY KEY `location` (`location`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
    `date` date DEFAULT NULL,
    `type` varchar(8) NOT NULL DEFAULT '',
    `location` varchar(64) NOT NULL DEFAULT '',
    `so2_max` decimal(5,2) NULL,
    `so2_max_time` varchar(8)  NULL,
    `so2_mean` decimal(5,2)  NULL,
    `co_max_8h` decimal(5,2)  NULL,
    `co_max_time` varchar(8)  NULL,
    `no2_max` decimal(5,2)  NULL,
    `no2_max_time` varchar(8)  NULL,
    `no2_mean` decimal(5,2)  NULL,
    `o3_max` decimal(5,2)  NULL,
    `o3_max_time` varchar(8)  NULL,
    `o3_mean` decimal(5,2)  NULL,
    `o3_max_8h` varchar(8)  NULL,
    `pm10` decimal(5,2)  NULL,
    `c6h6` decimal(5,2)  NULL,
    KEY `location` (`location`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

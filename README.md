# Aria Siracusa

Sul territorio di Siracusa esistono due reti private (CIPA ed ENEL) e due reti pubbliche: 
1. **Rete ARPA Sicilia** formata da n. 3 stazioni Megara , C.da Marcellino e Villa Augusta
2. **Rete Libero Consorzio Comunale di Siracusa** formata da 13 stazioni divise in due categorie
  - Rete urbana di Siracusa (5 stazioni – Acquedotto, Pantheon, Specchi, Teracati , Scala Greca)
  - Rete industriale di Siracusa (9 stazioni- Scala Greca, Augusta, Ciapi, Priolo, Melilli, S.Cusumano, Belvedere, Priolo Scuola, Augusta Monte Tauro) 
La stazione di Scala Greca fa parte sia della rete urbana sia di quella industriale.

I dati delle stazioni della rete di ARPA Sicilia sono consultabili sul sito http://qualitadellaria.arpa.sicilia.it:8080/

Sempre sul [sito di ARPA Sicilia](https://www.arpa.sicilia.it/temi-ambientali/aria/) è possibile trovare report _annuali_ e archivi dei dati in formato proprietario Excel. L'ultimo rapporto disponibile è del 2019.

La Rete Libero Consorzio pubblica i dati in formato chiuso PDF su questo sito FTP: ftp://213.82.231.194/

I file PDF sono raggiungibili dal sito della [Provincia](http://www.provincia.siracusa.it/informazioni_ambientali.php) e del [Comune](https://www.comune.siracusa.it/index.php/it/calendario-dei-report-giornalieri) di Siracusa.

Non ci è stato possibile trovare i dati in formato aperto su alcun sito istituzionale, né abbiamo potuto trovare alcuna visaulzizazione che ne permettesse una facile consultazione (potremmo esserci sbagliati, in caso fateci sapere).

Per questo motivo abbiamo voluto processare i PDF, estraendo tutte le rilevazioni dal 2014 in poi. I dati sono resi disponibili in formato aperto (CSV) all'indirizzo https://www.peacelink.it/datiariasiracusa che al momento rimanda a un archivio ospitato nell'infrastruttura di Google Cloud (questo reindirizzamento potrebbe cambiare in futuro, per questo invitiamo a memorizzare solo l'indirizzo presso il dominio peacelink.it)

I dati sono aggiornati quotidianamente, poco dopo la loro pubblicazione sul sito FTP della Rete Libero Consorzio.

## Sviluppi futuri

L'elenco attuale dei miglioramenti previsti è disponibile in questo progetto su Gitlab: https://gitlab.com/peacelink/aria-siracusa/-/boards

## Dettagli tecnici

Un semplice script in Python viene regolarmente eseguito su uno dei server di PeaceLink. Lo script [convert.py](convert.py) verifica la disponibilità di un nuovo bollettino sulla qualità dell'aria e se disponibile lo scarica ed estrae i dati dal file PDF.

Vengono  quindi generati due file CSV. Uno, relativo ai dati del giorno, viene trasferito su un bucket in Google Cloud Storage, da dove viene poi processato da Google BigQuery e aggiunto al database esistente. Un altro CSV, con i dati complessivi dell'anno, viene aggiornato e trasferito anch'esso su Google Cloud Storage, dove viene reso disponibile insieme ai dati degli anni precedenti a questo indirizzo: https://www.peacelink.it/datiariasiracusa

Un pannello in Google Data Studio offre la possibilità di consultare i dati e filtrare per data e per inquinante. Può essere consultato a questo indirizzo: https://www.peacelink.it/ariasiracusa

L'implementazione attuale si appoggia a Google Cloud sia per lo storage che per la visualizzazione. Non è stata una scelta dovuta a motivi tecnici ma solo di opportunità, e vorremmo implementare una soluzione dedicata in futuro. 

## Contatti

Francesco Iannuzzelli francesco@peacelink.org